from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import Message_Form
from .models import Activity

# Create your views here.

def schedule(request):
    response = dict()
    response['title'] = 'Add Schedule'
    response['message_form'] = Message_Form
    response['activities'] = Activity.objects.all().values()
    return render(request,'Schedule.html', response)


def addschedule(request):
    response = dict()
    
    # create a form instance and populate it with data from the request:
    form = Message_Form(request.POST or None)

    # check whether method is a POST and whether the form is valid
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['category'] = request.POST['category']
        response['location'] = request.POST['location']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        activity = Activity(name=response['name'], category=response['category'], location=response['location'], date=response['date'], time=response['time'])
        activity.save()
        return HttpResponseRedirect('schedule')

    # if method is a GET (or any other method) or form is invalid, create a blank form
    else:
        return HttpResponseRedirect('schedule')


def clearschedule(request):
    for i in Activity.objects.all():
        i.delete()
    return HttpResponseRedirect('schedule')