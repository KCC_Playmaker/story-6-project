from django.conf.urls import url
from django.urls import path
from .views import schedule, addschedule, clearschedule

app_name = "schedule_app"

urlpatterns = [
    path('schedule', schedule, name='schedule'),
    path('addschedule', addschedule, name='addschedule'), # for form submission
    path('clearschedule', clearschedule, name='clearschedule') # for schedule clear
]