from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Activity(models.Model):
    # author = models.ForeignKey(Person, on_delete = models.CASCADE)
    name = models.CharField(max_length=80)
    category = models.CharField(max_length=50)
    location = models.CharField(max_length=100)
    date = models.DateField(default=timezone.now)
    time = models.TimeField()