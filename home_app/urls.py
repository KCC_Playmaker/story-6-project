from django.conf.urls import url
from django.urls import path
from .views import home

app_name = "home_app"

urlpatterns = [
    path('', home, name='home'),
]