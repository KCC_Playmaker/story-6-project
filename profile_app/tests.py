from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from .views import *

# Create your tests here.

class Story6ProfileUnitTest(TestCase):

    def test_profile_response(self):
        response= self.client.get("/profile/")
        self.assertEqual(response.status_code,200)