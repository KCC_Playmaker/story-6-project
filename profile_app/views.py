from django.shortcuts import render

# Create your views here.

def profile(request):
    response = dict()
    return render(request, 'profile.html', response)