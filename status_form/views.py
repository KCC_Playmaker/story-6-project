from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import Status_Update_Form
from .models import StatusUpdate

import django.urls

# Create your views here.

response = {}
def status(request):
	response['title'] = 'Add Status'
	response['status_update_form'] = Status_Update_Form
	response['status_updates'] = StatusUpdate.objects.all().values().order_by('date').reverse()
	return render(request, 'status.html', response)

def add_status(request):
	# create a form instance and populate it with data from the request:
	form = Status_Update_Form(request.POST or None)

	# check whether method is a POST and whether the form is valid
	if(request.method == 'POST' and form.is_valid()):
		response['message'] = request.POST['message']
		status = StatusUpdate(message=response['message'])
		status.save()
	return HttpResponseRedirect(django.urls.reverse('status_form:status'))
