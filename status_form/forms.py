from django import forms

class Status_Update_Form(forms.Form):
    
    error_messages = {
        'required': 'Please fill this input',
    }
    
    message_attrs = {
        'class': 'form-control form-group',
        'placeholder': 'Enter new status message'
    }

    message = forms.CharField(label='Status message', required=True, max_length=300, widget=forms.TextInput(attrs=message_attrs))