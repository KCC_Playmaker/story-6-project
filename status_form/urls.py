from django.conf.urls import url
from django.urls import path
from .views import status, add_status

app_name = 'status_form'

urlpatterns = [
    path('', status, name='status'),
    path('add_status/', add_status, name='add_status'), # for form submission
]