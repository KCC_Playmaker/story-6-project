from django.test import TestCase
from django.test import Client
from django.utils import timezone
from django.urls import resolve
from .models import StatusUpdate
from .views import status

# Create your tests here.

class Story6StatusUnitTest(TestCase):
    
    def test_status_form_response(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

    def test_status_form_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'status.html')

    def test_status_form_status_function(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)
    
    def test_create_object_model(self):
        status_update = StatusUpdate.objects.create(message='This is a Unit Test.')
        status_count = StatusUpdate.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_POST_request(self):
        response = self.client.post('/status/add_status/', data={'message': 'Test status.', 'date': '2018-10-11 07:54:48.630021'})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/status/')
